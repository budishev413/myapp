import logo from "./logo.svg";
import "./App.css";
import agent from 'skywalking-backend-js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hello from My React app!
        </p>        
        <p>
          yc-k8s
        </p>
      </header>
    </div>
  );
}
agent.start();

export default App;
